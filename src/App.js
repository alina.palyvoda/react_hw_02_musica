import React from "react";
import styles from "./App.module.scss";
import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import Modal from "./components/Modal/Modal";

class App extends React.Component {
  state = {
    cards: [],
    favorites: [],
    isOpenModal: false,
    modalProps: {},
    cart: [],
  };

  async componentDidMount() {
    const data = await fetch("./data.json").then((res) => res.json());
    this.setState({ cards: data });
  }

  addToFavorites = (card) => {
    this.setState((current) => {
      const cards = [...current.cards];
      const i = cards.findIndex((el) => el.code === card.code);
      const favorites = [...current.favorites];
      const index = favorites.findIndex((el) => el.code === card.code);

      if (index === -1) {
        cards[i].isFavorite = true;
        favorites.push({ ...card });
      } else {
        cards[i].isFavorite = false;
        favorites.splice(index, 1);
      }

      localStorage.setItem("favorites", JSON.stringify(favorites));
      return { cards, favorites };
    });
  };

  setModalProps = (value) => {
    this.setState({ modalProps: value });
  };

  toggleModal = (value) => {
    this.setState({ isOpenModal: value });
  };

  addToCart = (card) => {
    this.setState((current) => {
      const cart = [...current.cart];
      cart.push({ ...card });
      localStorage.setItem("cart", JSON.stringify(cart));
      return { cart };
    });
  };

  render() {
    const { cards, favorites, isOpenModal, modalProps, cart } = this.state;

    return (
      <div className={styles.App}>
        <Header favorites={favorites} cart={cart} />
        <Main
          cards={cards}
          addToFavorites={this.addToFavorites}
          toggleModal={this.toggleModal}
          setModalProps={this.setModalProps}
        />
        {isOpenModal && (
          <Modal
            cards={cards}
            toggleModal={this.toggleModal}
            cart={cart}
            modalProps={modalProps}
            addToCart={this.addToCart}
          />
        )}
      </div>
    );
  }
}

export default App;
