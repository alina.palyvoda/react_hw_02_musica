import React from "react";
import styles from "./Main.module.scss";
import Card from "../Card/Card";
import PropTypes from "prop-types";

class Main extends React.PureComponent {
  render() {
    const { cards, addToFavorites, toggleModal, setModalProps } = this.props;

    return (
      <main className={styles.container}>
        {cards.map(({ name, price, src, code, color, isFavorite }) => (
          <Card
            key={code}
            title={name}
            imgSrc={src}
            price={price}
            color={color}
            code={code}
            addToFavorites={addToFavorites}
            isFavorite={isFavorite}
            toggleModal={toggleModal}
            setModalProps={setModalProps}
          />
        ))}
      </main>
    );
  }
}

Main.propTypes = {
  toggleModal: PropTypes.func,
  addToFavorites: PropTypes.func,
  cards: PropTypes.array,
  setModalProps: PropTypes.func,
};

Main.defaultProps = {
  toggleModal: () => {},
  setModalProps: () => {},
  addToFavorites: () => {},
  cards: [],
};

export default Main;
