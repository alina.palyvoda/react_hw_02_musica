import React from "react";
import PropTypes from "prop-types";
import styles from "./Button.module.scss";

class Button extends React.PureComponent {
  render() {
    const { children, type, onClick, className } = this.props;

    return (
      <button type={type} onClick={onClick} className={className}>
        {children}
      </button>
    );
  }
}

Button.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node),]).isRequired,
  onClick: PropTypes.func,
  className: PropTypes.string,
  type: PropTypes.oneOf(["button", "submit", "reset"]),
};

Button.defaultProps = {
  onClick: () => {},
  className: "",
  type: "button",
};

export default Button;
