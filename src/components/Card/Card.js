import React from "react";
import PropTypes from "prop-types";
import styles from "./Card.module.scss";
import Button from "../Button/Button";

class Card extends React.PureComponent {
  render() {
    const {
      title,
      imgSrc,
      price,
      color,
      code,
      isFavorite,
      addToFavorites,
      toggleModal,
      setModalProps,
    } = this.props;

    return (
      <div className={styles.container}>
        <img
          onClick={() => {
            addToFavorites({ title, imgSrc, price, code });}}
          className={styles.likeButton}
          alt={"like"}
          src={isFavorite ? "./images/like.png" : "./images/like-empty.png"}>
        </img>
        <a href="#" className={styles.wrapper}>
          <img className={styles.avatar} alt={title} src={imgSrc}></img>
          <h4 className={styles.title}>{title}</h4>
        </a>
        <span className={styles.price}>{price} грн</span>

        <Button
          onClick={() => {
            toggleModal(true);
            setModalProps({ title, price, code, imgSrc });
          }}
          className={styles.buyButton}>В корзину</Button>

        <div className={styles.description}>
          <span>Артикул: {code}</span>
          <span>Колір: {color}</span>
        </div>
      </div>
    );
  }
}

Card.propTypes = {
  title: PropTypes.string.isRequired,
  imgSrc: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  code: PropTypes.number.isRequired,
  color: PropTypes.string,
  isFavorite: PropTypes.bool,
  toggleModal: PropTypes.func,
  addToFavorites: PropTypes.func,
  setModalProps: PropTypes.func,
};

Card.defaultProps = {
  toggleModal: () => {},
  setModalProps: () => {},
  addToFavorites: () => {},
  isFavorite: false,
  color: "різнокольоровий",
};

export default Card;
