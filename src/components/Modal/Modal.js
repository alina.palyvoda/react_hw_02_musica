import React from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";

class Modal extends React.PureComponent {
  render() {
    const { modalProps, toggleModal, addToCart } = this.props;
    const { title, price, code, imgSrc } = modalProps;

    return (
      <div className={styles.modalContainer}>
        <div
          onClick={(e) => e.currentTarget === e.target && toggleModal(false)}
          className={styles.background}>
          <div className={styles.content}>
            <span onClick={() => {toggleModal(false)}}>x</span>
            <p>
              Ви дійсно хочете додати в корзину іграшку <br /> {title} за {price} грн?</p>
            <div>
              <button onClick={() => {
                  addToCart({ title, price, code, imgSrc });
                  toggleModal(false);
                }}
                type="submit">Yes</button>
              <button onClick={() => {toggleModal(false)}} type="submit">No</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  modalProps: PropTypes.shape({
    name: PropTypes.string,
    price: PropTypes.number,
    code: PropTypes.number,
    imgSrc: PropTypes.string,
  }),
  toggleModal: PropTypes.func,
  addToCart: PropTypes.func,
};

Modal.defaultProps = {
  toggleModal: () => {},
  addToCart: () => {},
  modalProps: {
    title: "",
    price: null,
    code: null,
    imgSrc: "",
  },
};

export default Modal;
