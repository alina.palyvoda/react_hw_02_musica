import React from "react";
import PropTypes from "prop-types";
import styles from "./Header.module.scss";

class Header extends React.PureComponent {
  render() {
    const { favorites, cart } = this.props;

    return (
      <header className={styles.container}>
        <div className={styles.contacts}>
          <a href="tel:+38(000)000-00-00">+38(000)000-00-00</a>
          <a href="mailto:softly@email.com">softly@email.com</a>
        </div>
        <a className={styles.logo} href="#">
          <h1 className={styles.logoName}>SOFTLY</h1>
          <span>магазин м'яких іграшок</span>
        </a>

        <div>
          <a className={styles.favourites}>
            <img src="./images/favouritesIcon.png"></img>
            <span>{favorites.length}</span>
          </a>
          <a className={styles.cart}>
            <img src="./images/cartIcon.png"></img>
            <span>{cart.length}</span>
          </a>
        </div>
      </header>
    );
  }
}

Header.propTypes = {
  favorites: PropTypes.array,
  cart: PropTypes.array,
};

Header.defaultProps = {
  favorites: [],
  cart: [],
};

export default Header;
